
package webfly.Utileria;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class FondoPanel extends JPanel{
    private Image Fondo;
    String URL;
    public FondoPanel(String url) {
        URL = url;
    }
    @Override
    public void paint(Graphics g){
        System.out.println(URL);
        Fondo = new ImageIcon(getClass().getResource(URL)).getImage();
        g.drawImage(Fondo,0,0,getWidth(),getHeight(),this);
        setOpaque(false);
        super.paint(g);
    }
}
    