/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.BD;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Santiago
 */
@Entity
@Table(name = "vuelo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vuelo.findAll", query = "SELECT v FROM Vuelo v")
    , @NamedQuery(name = "Vuelo.findByVueloId", query = "SELECT v FROM Vuelo v WHERE v.vueloId = :vueloId")
    , @NamedQuery(name = "Vuelo.findByVueloPrecio", query = "SELECT v FROM Vuelo v WHERE v.vueloPrecio = :vueloPrecio")
    , @NamedQuery(name = "Vuelo.findByVueloFecha", query = "SELECT v FROM Vuelo v WHERE v.vueloFecha = :vueloFecha")})
public class Vuelo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "VueloId")
    private Integer vueloId;
    @Basic(optional = false)
    @Column(name = "VueloPrecio")
    private int vueloPrecio;
    @Basic(optional = false)
    @Column(name = "VueloFecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vueloFecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vueloId")
    private Collection<Compra> compraCollection;
    @JoinColumn(name = "AvionId", referencedColumnName = "AvionId")
    @ManyToOne(optional = false)
    private Avion avionId;
    @JoinColumn(name = "CiudadIdOrigen", referencedColumnName = "CiudadId")
    @ManyToOne(optional = false)
    private Ciudades ciudadIdOrigen;
    @JoinColumn(name = "CiudadIdDestino", referencedColumnName = "CiudadId")
    @ManyToOne(optional = false)
    private Ciudades ciudadIdDestino;

    public Vuelo() {
    }

    public Vuelo(Integer vueloId) {
        this.vueloId = vueloId;
    }

    public Vuelo(Integer vueloId, int vueloPrecio, Date vueloFecha) {
        this.vueloId = vueloId;
        this.vueloPrecio = vueloPrecio;
        this.vueloFecha = vueloFecha;
    }

    public Integer getVueloId() {
        return vueloId;
    }

    public void setVueloId(Integer vueloId) {
        this.vueloId = vueloId;
    }

    public int getVueloPrecio() {
        return vueloPrecio;
    }

    public void setVueloPrecio(int vueloPrecio) {
        this.vueloPrecio = vueloPrecio;
    }

    public Date getVueloFecha() {
        return vueloFecha;
    }

    public void setVueloFecha(Date vueloFecha) {
        this.vueloFecha = vueloFecha;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    public Avion getAvionId() {
        return avionId;
    }

    public void setAvionId(Avion avionId) {
        this.avionId = avionId;
    }

    public Ciudades getCiudadIdOrigen() {
        return ciudadIdOrigen;
    }

    public void setCiudadIdOrigen(Ciudades ciudadIdOrigen) {
        this.ciudadIdOrigen = ciudadIdOrigen;
    }

    public Ciudades getCiudadIdDestino() {
        return ciudadIdDestino;
    }

    public void setCiudadIdDestino(Ciudades ciudadIdDestino) {
        this.ciudadIdDestino = ciudadIdDestino;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vueloId != null ? vueloId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vuelo)) {
            return false;
        }
        Vuelo other = (Vuelo) object;
        if ((this.vueloId == null && other.vueloId != null) || (this.vueloId != null && !this.vueloId.equals(other.vueloId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "webfly.BD.Vuelo[ vueloId=" + vueloId + " ]";
    }
    
}
