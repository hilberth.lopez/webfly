/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.BD;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Santiago
 */
@Entity
@Table(name = "aerolinea")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aerolinea.findAll", query = "SELECT a FROM Aerolinea a")
    , @NamedQuery(name = "Aerolinea.findByAerolineaRut", query = "SELECT a FROM Aerolinea a WHERE a.aerolineaRut = :aerolineaRut")
    , @NamedQuery(name = "Aerolinea.findByAerolineaNombre", query = "SELECT a FROM Aerolinea a WHERE a.aerolineaNombre = :aerolineaNombre")
    , @NamedQuery(name = "Aerolinea.findByAerolineaTelefono", query = "SELECT a FROM Aerolinea a WHERE a.aerolineaTelefono = :aerolineaTelefono")
    , @NamedQuery(name = "Aerolinea.findByAerolineaLogo", query = "SELECT a FROM Aerolinea a WHERE a.aerolineaLogo = :aerolineaLogo")})
public class Aerolinea implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "AerolineaRut")
    private Integer aerolineaRut;
    @Column(name = "AerolineaNombre")
    private String aerolineaNombre;
    @Column(name = "AerolineaTelefono")
    private Integer aerolineaTelefono;
    @Column(name = "AerolineaLogo")
    private String aerolineaLogo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "aerolineaRut")
    private Collection<Avion> avionCollection;

    public Aerolinea() {
    }

    public Aerolinea(Integer aerolineaRut) {
        this.aerolineaRut = aerolineaRut;
    }

    public Integer getAerolineaRut() {
        return aerolineaRut;
    }

    public void setAerolineaRut(Integer aerolineaRut) {
        this.aerolineaRut = aerolineaRut;
    }

    public String getAerolineaNombre() {
        return aerolineaNombre;
    }

    public void setAerolineaNombre(String aerolineaNombre) {
        this.aerolineaNombre = aerolineaNombre;
    }

    public Integer getAerolineaTelefono() {
        return aerolineaTelefono;
    }

    public void setAerolineaTelefono(Integer aerolineaTelefono) {
        this.aerolineaTelefono = aerolineaTelefono;
    }

    public String getAerolineaLogo() {
        return aerolineaLogo;
    }

    public void setAerolineaLogo(String aerolineaLogo) {
        this.aerolineaLogo = aerolineaLogo;
    }

    @XmlTransient
    public Collection<Avion> getAvionCollection() {
        return avionCollection;
    }

    public void setAvionCollection(Collection<Avion> avionCollection) {
        this.avionCollection = avionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aerolineaRut != null ? aerolineaRut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aerolinea)) {
            return false;
        }
        Aerolinea other = (Aerolinea) object;
        if ((this.aerolineaRut == null && other.aerolineaRut != null) || (this.aerolineaRut != null && !this.aerolineaRut.equals(other.aerolineaRut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "webfly.BD.Aerolinea[ aerolineaRut=" + aerolineaRut + " ]";
    }
    
}
