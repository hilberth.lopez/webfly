/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.BD;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Luis Santiago
 */
@Entity
@Table(name = "compra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compra.findAll", query = "SELECT c FROM Compra c")
    , @NamedQuery(name = "Compra.findByCompraId", query = "SELECT c FROM Compra c WHERE c.compraId = :compraId")
    , @NamedQuery(name = "Compra.findByCompraAsiento", query = "SELECT c FROM Compra c WHERE c.compraAsiento = :compraAsiento")
    , @NamedQuery(name = "Compra.findByCompraFecha", query = "SELECT c FROM Compra c WHERE c.compraFecha = :compraFecha")
    , @NamedQuery(name = "Compra.findByCompraHora", query = "SELECT c FROM Compra c WHERE c.compraHora = :compraHora")})
public class Compra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CompraId")
    private Integer compraId;
    @Basic(optional = false)
    @Column(name = "CompraAsiento")
    private String compraAsiento;
    @Basic(optional = false)
    @Column(name = "CompraFecha")
    @Temporal(TemporalType.DATE)
    private Date compraFecha;
    @Basic(optional = false)
    @Column(name = "CompraHora")
    @Temporal(TemporalType.TIME)
    private Date compraHora;
    @JoinColumn(name = "VueloId", referencedColumnName = "VueloId")
    @ManyToOne(optional = false)
    private Vuelo vueloId;
    @JoinColumn(name = "UsuarioVendedorCedula", referencedColumnName = "UsuarioCedula")
    @ManyToOne(optional = false)
    private Usuario usuarioVendedorCedula;
    @JoinColumn(name = "UsuarioClienteCedula", referencedColumnName = "UsuarioCedula")
    @ManyToOne(optional = false)
    private Usuario usuarioClienteCedula;

    public Compra() {
    }

    public Compra(Integer compraId) {
        this.compraId = compraId;
    }

    public Compra(Integer compraId, String compraAsiento, Date compraFecha, Date compraHora) {
        this.compraId = compraId;
        this.compraAsiento = compraAsiento;
        this.compraFecha = compraFecha;
        this.compraHora = compraHora;
    }

    public Integer getCompraId() {
        return compraId;
    }

    public void setCompraId(Integer compraId) {
        this.compraId = compraId;
    }

    public String getCompraAsiento() {
        return compraAsiento;
    }

    public void setCompraAsiento(String compraAsiento) {
        this.compraAsiento = compraAsiento;
    }

    public Date getCompraFecha() {
        return compraFecha;
    }

    public void setCompraFecha(Date compraFecha) {
        this.compraFecha = compraFecha;
    }

    public Date getCompraHora() {
        return compraHora;
    }

    public void setCompraHora(Date compraHora) {
        this.compraHora = compraHora;
    }

    public Vuelo getVueloId() {
        return vueloId;
    }

    public void setVueloId(Vuelo vueloId) {
        this.vueloId = vueloId;
    }

    public Usuario getUsuarioVendedorCedula() {
        return usuarioVendedorCedula;
    }

    public void setUsuarioVendedorCedula(Usuario usuarioVendedorCedula) {
        this.usuarioVendedorCedula = usuarioVendedorCedula;
    }

    public Usuario getUsuarioClienteCedula() {
        return usuarioClienteCedula;
    }

    public void setUsuarioClienteCedula(Usuario usuarioClienteCedula) {
        this.usuarioClienteCedula = usuarioClienteCedula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compraId != null ? compraId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compra)) {
            return false;
        }
        Compra other = (Compra) object;
        if ((this.compraId == null && other.compraId != null) || (this.compraId != null && !this.compraId.equals(other.compraId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "webfly.BD.Compra[ compraId=" + compraId + " ]";
    }
    
}
