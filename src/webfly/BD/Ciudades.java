/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.BD;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Santiago
 */
@Entity
@Table(name = "ciudades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ciudades.findAll", query = "SELECT c FROM Ciudades c")
    , @NamedQuery(name = "Ciudades.findByCiudadId", query = "SELECT c FROM Ciudades c WHERE c.ciudadId = :ciudadId")
    , @NamedQuery(name = "Ciudades.findByCiudadNombre", query = "SELECT c FROM Ciudades c WHERE c.ciudadNombre = :ciudadNombre")
    , @NamedQuery(name = "Ciudades.findByCiudadDepartamentoNombre", query = "SELECT c FROM Ciudades c WHERE c.ciudadDepartamentoNombre = :ciudadDepartamentoNombre")
    , @NamedQuery(name = "Ciudades.findByCiudadEstado", query = "SELECT c FROM Ciudades c WHERE c.ciudadEstado = :ciudadEstado")
    , @NamedQuery(name = "Ciudades.findByCiudadImagen", query = "SELECT c FROM Ciudades c WHERE c.ciudadImagen = :ciudadImagen")})
public class Ciudades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CiudadId")
    private Integer ciudadId;
    @Basic(optional = false)
    @Column(name = "CiudadNombre")
    private String ciudadNombre;
    @Basic(optional = false)
    @Column(name = "CiudadDepartamentoNombre")
    private String ciudadDepartamentoNombre;
    @Basic(optional = false)
    @Column(name = "CiudadEstado")
    private String ciudadEstado;
    @Column(name = "CiudadImagen")
    private String ciudadImagen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ciudadIdOrigen")
    private Collection<Vuelo> vueloCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ciudadIdDestino")
    private Collection<Vuelo> vueloCollection1;

    public Ciudades() {
    }

    public Ciudades(Integer ciudadId) {
        this.ciudadId = ciudadId;
    }

    public Ciudades(Integer ciudadId, String ciudadNombre, String ciudadDepartamentoNombre, String ciudadEstado) {
        this.ciudadId = ciudadId;
        this.ciudadNombre = ciudadNombre;
        this.ciudadDepartamentoNombre = ciudadDepartamentoNombre;
        this.ciudadEstado = ciudadEstado;
    }

    public Integer getCiudadId() {
        return ciudadId;
    }

    public void setCiudadId(Integer ciudadId) {
        this.ciudadId = ciudadId;
    }

    public String getCiudadNombre() {
        return ciudadNombre;
    }

    public void setCiudadNombre(String ciudadNombre) {
        this.ciudadNombre = ciudadNombre;
    }

    public String getCiudadDepartamentoNombre() {
        return ciudadDepartamentoNombre;
    }

    public void setCiudadDepartamentoNombre(String ciudadDepartamentoNombre) {
        this.ciudadDepartamentoNombre = ciudadDepartamentoNombre;
    }

    public String getCiudadEstado() {
        return ciudadEstado;
    }

    public void setCiudadEstado(String ciudadEstado) {
        this.ciudadEstado = ciudadEstado;
    }

    public String getCiudadImagen() {
        return ciudadImagen;
    }

    public void setCiudadImagen(String ciudadImagen) {
        this.ciudadImagen = ciudadImagen;
    }

    @XmlTransient
    public Collection<Vuelo> getVueloCollection() {
        return vueloCollection;
    }

    public void setVueloCollection(Collection<Vuelo> vueloCollection) {
        this.vueloCollection = vueloCollection;
    }

    @XmlTransient
    public Collection<Vuelo> getVueloCollection1() {
        return vueloCollection1;
    }

    public void setVueloCollection1(Collection<Vuelo> vueloCollection1) {
        this.vueloCollection1 = vueloCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ciudadId != null ? ciudadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ciudades)) {
            return false;
        }
        Ciudades other = (Ciudades) object;
        if ((this.ciudadId == null && other.ciudadId != null) || (this.ciudadId != null && !this.ciudadId.equals(other.ciudadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "webfly.BD.Ciudades[ ciudadId=" + ciudadId + " ]";
    }
    
}
