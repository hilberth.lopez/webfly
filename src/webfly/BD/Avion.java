/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.BD;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Santiago
 */
@Entity
@Table(name = "avion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Avion.findAll", query = "SELECT a FROM Avion a")
    , @NamedQuery(name = "Avion.findByAvionId", query = "SELECT a FROM Avion a WHERE a.avionId = :avionId")
    , @NamedQuery(name = "Avion.findByAvionNombre", query = "SELECT a FROM Avion a WHERE a.avionNombre = :avionNombre")
    , @NamedQuery(name = "Avion.findByAvionTipo", query = "SELECT a FROM Avion a WHERE a.avionTipo = :avionTipo")})
public class Avion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "AvionId")
    private Integer avionId;
    @Basic(optional = false)
    @Column(name = "AvionNombre")
    private String avionNombre;
    @Basic(optional = false)
    @Column(name = "AvionTipo")
    private String avionTipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "avionId")
    private Collection<Vuelo> vueloCollection;
    @JoinColumn(name = "AerolineaRut", referencedColumnName = "AerolineaRut")
    @ManyToOne(optional = false)
    private Aerolinea aerolineaRut;

    public Avion() {
    }

    public Avion(Integer avionId) {
        this.avionId = avionId;
    }

    public Avion(Integer avionId, String avionNombre, String avionTipo) {
        this.avionId = avionId;
        this.avionNombre = avionNombre;
        this.avionTipo = avionTipo;
    }

    public Integer getAvionId() {
        return avionId;
    }

    public void setAvionId(Integer avionId) {
        this.avionId = avionId;
    }

    public String getAvionNombre() {
        return avionNombre;
    }

    public void setAvionNombre(String avionNombre) {
        this.avionNombre = avionNombre;
    }

    public String getAvionTipo() {
        return avionTipo;
    }

    public void setAvionTipo(String avionTipo) {
        this.avionTipo = avionTipo;
    }

    @XmlTransient
    public Collection<Vuelo> getVueloCollection() {
        return vueloCollection;
    }

    public void setVueloCollection(Collection<Vuelo> vueloCollection) {
        this.vueloCollection = vueloCollection;
    }

    public Aerolinea getAerolineaRut() {
        return aerolineaRut;
    }

    public void setAerolineaRut(Aerolinea aerolineaRut) {
        this.aerolineaRut = aerolineaRut;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (avionId != null ? avionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Avion)) {
            return false;
        }
        Avion other = (Avion) object;
        if ((this.avionId == null && other.avionId != null) || (this.avionId != null && !this.avionId.equals(other.avionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "webfly.BD.Avion[ avionId=" + avionId + " ]";
    }
    
}
