/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.BD;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luis Santiago
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByUsuarioCedula", query = "SELECT u FROM Usuario u WHERE u.usuarioCedula = :usuarioCedula")
    , @NamedQuery(name = "Usuario.findByUsuarioNombre", query = "SELECT u FROM Usuario u WHERE u.usuarioNombre = :usuarioNombre")
    , @NamedQuery(name = "Usuario.findByUsuarioApellido", query = "SELECT u FROM Usuario u WHERE u.usuarioApellido = :usuarioApellido")
    , @NamedQuery(name = "Usuario.findByUsuarioCorreo", query = "SELECT u FROM Usuario u WHERE u.usuarioCorreo = :usuarioCorreo")
    , @NamedQuery(name = "Usuario.findByUsuarioTelefono", query = "SELECT u FROM Usuario u WHERE u.usuarioTelefono = :usuarioTelefono")
    , @NamedQuery(name = "Usuario.findByUsuarioContrase\u00f1a", query = "SELECT u FROM Usuario u WHERE u.usuarioContrase\u00f1a = :usuarioContrase\u00f1a")
    , @NamedQuery(name = "Usuario.findByUsuarioRol", query = "SELECT u FROM Usuario u WHERE u.usuarioRol = :usuarioRol")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "UsuarioCedula")
    private Integer usuarioCedula;
    @Basic(optional = false)
    @Column(name = "UsuarioNombre")
    private String usuarioNombre;
    @Basic(optional = false)
    @Column(name = "UsuarioApellido")
    private String usuarioApellido;
    @Basic(optional = false)
    @Column(name = "UsuarioCorreo")
    private String usuarioCorreo;
    @Basic(optional = false)
    @Column(name = "UsuarioTelefono")
    private int usuarioTelefono;
    @Basic(optional = false)
    @Column(name = "UsuarioContrase\u00f1a")
    private String usuarioContraseña;
    @Basic(optional = false)
    @Column(name = "UsuarioRol")
    private String usuarioRol;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuarioVendedorCedula")
    private Collection<Compra> compraCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuarioClienteCedula")
    private Collection<Compra> compraCollection1;

    public Usuario() {
    }

    public Usuario(Integer usuarioCedula) {
        this.usuarioCedula = usuarioCedula;
    }

    public Usuario(Integer usuarioCedula, String usuarioNombre, String usuarioApellido, String usuarioCorreo, int usuarioTelefono, String usuarioContraseña, String usuarioRol) {
        this.usuarioCedula = usuarioCedula;
        this.usuarioNombre = usuarioNombre;
        this.usuarioApellido = usuarioApellido;
        this.usuarioCorreo = usuarioCorreo;
        this.usuarioTelefono = usuarioTelefono;
        this.usuarioContraseña = usuarioContraseña;
        this.usuarioRol = usuarioRol;
    }

    public Integer getUsuarioCedula() {
        return usuarioCedula;
    }

    public void setUsuarioCedula(Integer usuarioCedula) {
        this.usuarioCedula = usuarioCedula;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public String getUsuarioApellido() {
        return usuarioApellido;
    }

    public void setUsuarioApellido(String usuarioApellido) {
        this.usuarioApellido = usuarioApellido;
    }

    public String getUsuarioCorreo() {
        return usuarioCorreo;
    }

    public void setUsuarioCorreo(String usuarioCorreo) {
        this.usuarioCorreo = usuarioCorreo;
    }

    public int getUsuarioTelefono() {
        return usuarioTelefono;
    }

    public void setUsuarioTelefono(int usuarioTelefono) {
        this.usuarioTelefono = usuarioTelefono;
    }

    public String getUsuarioContraseña() {
        return usuarioContraseña;
    }

    public void setUsuarioContraseña(String usuarioContraseña) {
        this.usuarioContraseña = usuarioContraseña;
    }

    public String getUsuarioRol() {
        return usuarioRol;
    }

    public void setUsuarioRol(String usuarioRol) {
        this.usuarioRol = usuarioRol;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection1() {
        return compraCollection1;
    }

    public void setCompraCollection1(Collection<Compra> compraCollection1) {
        this.compraCollection1 = compraCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioCedula != null ? usuarioCedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuarioCedula == null && other.usuarioCedula != null) || (this.usuarioCedula != null && !this.usuarioCedula.equals(other.usuarioCedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "webfly.BD.Usuario[ usuarioCedula=" + usuarioCedula + " ]";
    }
    
}
