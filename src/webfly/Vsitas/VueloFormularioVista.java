
package webfly.Vsitas;
import webfly.Utileria.FondoPanel;

public class VueloFormularioVista extends javax.swing.JFrame {


    public VueloFormularioVista() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPFondo = new FondoPanel("/webfly/Imagenes/airplane.jpg");
        jPFormulario = new javax.swing.JPanel();
        jLTitulo = new javax.swing.JLabel();
        jLAerolinea = new javax.swing.JLabel();
        jLOrigen = new javax.swing.JLabel();
        jLDestino = new javax.swing.JLabel();
        jLFecha = new javax.swing.JLabel();
        jLPrecio = new javax.swing.JLabel();
        jCBAerolinea = new javax.swing.JComboBox<>();
        jCBOrigen = new javax.swing.JComboBox<>();
        jCBDestino = new javax.swing.JComboBox<>();
        jTFPrecio = new javax.swing.JTextField();
        jBGuardar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        jCBDia = new javax.swing.JComboBox<>();
        jCBMes = new javax.swing.JComboBox<>();
        jCBAno = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setExtendedState(6);

        jPFormulario.setBackground(new java.awt.Color(255, 255, 255));

        jLTitulo.setFont(new java.awt.Font("Corbel", 0, 30)); // NOI18N
        jLTitulo.setForeground(new java.awt.Color(0, 153, 153));
        jLTitulo.setText("Formulario para registrar un vuelo");

        jLAerolinea.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLAerolinea.setText("Aerolínea");

        jLOrigen.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLOrigen.setText("Origen");

        jLDestino.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLDestino.setText("Destino");

        jLFecha.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLFecha.setText("Fecha");

        jLPrecio.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLPrecio.setText("Precio");

        jCBAerolinea.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jCBAerolinea.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCBAerolinea.setOpaque(false);

        jCBOrigen.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jCBOrigen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCBOrigen.setOpaque(false);

        jCBDestino.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jCBDestino.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCBDestino.setAlignmentX(0.0F);
        jCBDestino.setAlignmentY(0.0F);
        jCBDestino.setFocusable(false);
        jCBDestino.setLightWeightPopupEnabled(false);

        jTFPrecio.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N

        jBGuardar.setBackground(new java.awt.Color(0, 153, 153));
        jBGuardar.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jBGuardar.setForeground(new java.awt.Color(255, 255, 255));
        jBGuardar.setText("Guardar");
        jBGuardar.setOpaque(false);

        jBCancelar.setBackground(new java.awt.Color(0, 153, 153));
        jBCancelar.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jBCancelar.setForeground(new java.awt.Color(255, 255, 255));
        jBCancelar.setText("Cancelar");
        jBCancelar.setOpaque(false);
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        jCBDia.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jCBDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "dd", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        jCBDia.setToolTipText("");

        jCBMes.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jCBMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "mm", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));

        jCBAno.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jCBAno.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "aaaa", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040" }));

        javax.swing.GroupLayout jPFormularioLayout = new javax.swing.GroupLayout(jPFormulario);
        jPFormulario.setLayout(jPFormularioLayout);
        jPFormularioLayout.setHorizontalGroup(
            jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularioLayout.createSequentialGroup()
                .addGap(129, 129, 129)
                .addComponent(jBGuardar)
                .addGap(64, 64, 64)
                .addComponent(jBCancelar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPFormularioLayout.createSequentialGroup()
                .addContainerGap(53, Short.MAX_VALUE)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLOrigen)
                    .addComponent(jLTitulo)
                    .addGroup(jPFormularioLayout.createSequentialGroup()
                        .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLAerolinea)
                            .addComponent(jLDestino))
                        .addGap(18, 18, 18)
                        .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jCBDestino, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jCBAerolinea, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jCBOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPFormularioLayout.createSequentialGroup()
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLFecha)
                                .addComponent(jLPrecio))
                            .addGap(44, 44, 44)
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPFormularioLayout.createSequentialGroup()
                                    .addComponent(jCBDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jCBMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jCBAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jTFPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(42, 42, 42))
        );
        jPFormularioLayout.setVerticalGroup(
            jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularioLayout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(jLTitulo)
                .addGap(50, 50, 50)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLAerolinea)
                    .addComponent(jCBAerolinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLDestino)
                    .addComponent(jCBDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLOrigen)
                    .addComponent(jCBOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLFecha)
                    .addComponent(jCBDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCBMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCBAno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLPrecio)
                    .addComponent(jTFPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBGuardar)
                    .addComponent(jBCancelar))
                .addContainerGap(84, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPFondoLayout = new javax.swing.GroupLayout(jPFondo);
        jPFondo.setLayout(jPFondoLayout);
        jPFondoLayout.setHorizontalGroup(
            jPFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPFondoLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addComponent(jPFormulario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
        jPFondoLayout.setVerticalGroup(
            jPFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFondo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_jBCancelarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBGuardar;
    private javax.swing.JComboBox<String> jCBAerolinea;
    private javax.swing.JComboBox<String> jCBAno;
    private javax.swing.JComboBox<String> jCBDestino;
    private javax.swing.JComboBox<String> jCBDia;
    private javax.swing.JComboBox<String> jCBMes;
    private javax.swing.JComboBox<String> jCBOrigen;
    private javax.swing.JLabel jLAerolinea;
    private javax.swing.JLabel jLDestino;
    private javax.swing.JLabel jLFecha;
    private javax.swing.JLabel jLOrigen;
    private javax.swing.JLabel jLPrecio;
    private javax.swing.JLabel jLTitulo;
    private javax.swing.JPanel jPFondo;
    private javax.swing.JPanel jPFormulario;
    private javax.swing.JTextField jTFPrecio;
    // End of variables declaration//GEN-END:variables
}
