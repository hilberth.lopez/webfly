
package webfly.Vsitas;

public class ConfiguracionVista extends javax.swing.JFrame {


    public ConfiguracionVista() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        JBregistraraerolinea = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        JBregistrarciudad = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        JBregistrarusuario = new javax.swing.JButton();
        JBlogin = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        JBregistrarvuelo = new javax.swing.JButton();
        JBvuelos = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("CONFIGURACION");

        jLabel1.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 153));
        jLabel1.setText("Configuración");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 153, 153));
        jLabel4.setText("Aerolinea");

        JBregistraraerolinea.setBackground(new java.awt.Color(255, 255, 255));
        JBregistraraerolinea.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        JBregistraraerolinea.setText("Registrar una aerolinea");
        JBregistraraerolinea.setBorder(null);
        JBregistraraerolinea.setBorderPainted(false);
        JBregistraraerolinea.setContentAreaFilled(false);
        JBregistraraerolinea.setDefaultCapable(false);
        JBregistraraerolinea.setFocusPainted(false);
        JBregistraraerolinea.setFocusable(false);
        JBregistraraerolinea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBregistraraerolineaActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 153));
        jLabel6.setText("Ciudad");

        JBregistrarciudad.setBackground(new java.awt.Color(255, 255, 255));
        JBregistrarciudad.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        JBregistrarciudad.setText("Registrar una ciudad");
        JBregistrarciudad.setBorder(null);
        JBregistrarciudad.setBorderPainted(false);
        JBregistrarciudad.setContentAreaFilled(false);
        JBregistrarciudad.setDefaultCapable(false);
        JBregistrarciudad.setFocusPainted(false);
        JBregistrarciudad.setFocusable(false);
        JBregistrarciudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBregistrarciudadActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setText("Usuarios");

        JBregistrarusuario.setBackground(new java.awt.Color(255, 255, 255));
        JBregistrarusuario.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        JBregistrarusuario.setText("Registrar un usuario");
        JBregistrarusuario.setBorder(null);
        JBregistrarusuario.setBorderPainted(false);
        JBregistrarusuario.setContentAreaFilled(false);
        JBregistrarusuario.setDefaultCapable(false);
        JBregistrarusuario.setFocusPainted(false);
        JBregistrarusuario.setFocusable(false);
        JBregistrarusuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBregistrarusuarioActionPerformed(evt);
            }
        });

        JBlogin.setBackground(new java.awt.Color(255, 255, 255));
        JBlogin.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        JBlogin.setText("Login ");
        JBlogin.setBorder(null);
        JBlogin.setBorderPainted(false);
        JBlogin.setContentAreaFilled(false);
        JBlogin.setDefaultCapable(false);
        JBlogin.setFocusPainted(false);
        JBlogin.setFocusable(false);
        JBlogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBloginActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 153, 153));
        jLabel3.setText("Vuelos");

        JBregistrarvuelo.setBackground(new java.awt.Color(255, 255, 255));
        JBregistrarvuelo.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        JBregistrarvuelo.setText("Registrar un vuelo");
        JBregistrarvuelo.setBorder(null);
        JBregistrarvuelo.setBorderPainted(false);
        JBregistrarvuelo.setContentAreaFilled(false);
        JBregistrarvuelo.setDefaultCapable(false);
        JBregistrarvuelo.setFocusPainted(false);
        JBregistrarvuelo.setFocusable(false);
        JBregistrarvuelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBregistrarvueloActionPerformed(evt);
            }
        });

        JBvuelos.setBackground(new java.awt.Color(255, 255, 255));
        JBvuelos.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        JBvuelos.setText("Vuelos");
        JBvuelos.setBorder(null);
        JBvuelos.setBorderPainted(false);
        JBvuelos.setContentAreaFilled(false);
        JBvuelos.setDefaultCapable(false);
        JBvuelos.setFocusPainted(false);
        JBvuelos.setFocusable(false);
        JBvuelos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBvuelosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addGap(202, 202, 202))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(JBregistraraerolinea)
                                .addGap(302, 302, 302)
                                .addComponent(JBregistrarciudad))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JBregistrarusuario)
                                    .addComponent(JBlogin)
                                    .addComponent(jLabel5))
                                .addGap(329, 329, 329)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JBregistrarvuelo)
                                    .addComponent(jLabel3)
                                    .addComponent(JBvuelos))))
                        .addContainerGap(95, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addGap(4, 4, 4)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(JBregistraraerolinea)
                        .addGap(83, 83, 83)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(JBregistrarusuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JBlogin))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(JBregistrarciudad)
                        .addGap(75, 75, 75)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(JBregistrarvuelo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JBvuelos)))
                .addContainerGap(118, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel1)))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 37, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JBregistrarciudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBregistrarciudadActionPerformed
//CiudadFormularioVista ciudad = new CiudadFormularioVista();
//ciudad.setVisible(true);
       
    }//GEN-LAST:event_JBregistrarciudadActionPerformed

    private void JBregistrarusuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBregistrarusuarioActionPerformed
UsuarioRegistrarVista registrar = new UsuarioRegistrarVista();
registrar.setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_JBregistrarusuarioActionPerformed

    private void JBregistraraerolineaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBregistraraerolineaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JBregistraraerolineaActionPerformed

    private void JBloginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBloginActionPerformed
LoginVista logueo = new LoginVista();
logueo.setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_JBloginActionPerformed

    private void JBregistrarvueloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBregistrarvueloActionPerformed
VueloFormularioVista vuelo = new VueloFormularioVista();
vuelo.setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_JBregistrarvueloActionPerformed

    private void JBvuelosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBvuelosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JBvuelosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBlogin;
    private javax.swing.JButton JBregistraraerolinea;
    private javax.swing.JButton JBregistrarciudad;
    private javax.swing.JButton JBregistrarusuario;
    private javax.swing.JButton JBregistrarvuelo;
    private javax.swing.JButton JBvuelos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
