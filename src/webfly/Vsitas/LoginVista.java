
package webfly.Vsitas;
import webfly.Utileria.FondoPanel;

public class LoginVista extends javax.swing.JFrame {

    public LoginVista() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPFondo = new FondoPanel("/webfly/Imagenes/fonfoLogin.jpg");
        jPFormulario = new javax.swing.JPanel();
        jLTitulo = new javax.swing.JLabel();
        jLTexto = new javax.swing.JLabel();
        jLUsuario = new javax.swing.JLabel();
        jLContraseña = new javax.swing.JLabel();
        jTFUsuario = new javax.swing.JTextField();
        jTFContrasena = new javax.swing.JTextField();
        JBOlvideContrasena = new javax.swing.JButton();
        jBRegistrarme = new javax.swing.JButton();
        jBIngresar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Iniciar Seción");
        setExtendedState(6);
        setMinimumSize(new java.awt.Dimension(469, 538));

        jPFormulario.setBackground(new java.awt.Color(255, 255, 255));
        jPFormulario.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLTitulo.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLTitulo.setForeground(new java.awt.Color(0, 153, 153));
        jLTitulo.setText("Agencia de Viajes ");

        jLTexto.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLTexto.setText("Ingresa los datos ");

        jLUsuario.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLUsuario.setText("Usuario");

        jLContraseña.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLContraseña.setText("Contraseña");

        jTFUsuario.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jTFContrasena.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        JBOlvideContrasena.setBackground(new java.awt.Color(255, 255, 255));
        JBOlvideContrasena.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        JBOlvideContrasena.setForeground(new java.awt.Color(0, 153, 153));
        JBOlvideContrasena.setText("Olvidé mi contraseña");
        JBOlvideContrasena.setBorder(null);
        JBOlvideContrasena.setBorderPainted(false);
        JBOlvideContrasena.setContentAreaFilled(false);

        jBRegistrarme.setBackground(new java.awt.Color(255, 255, 255));
        jBRegistrarme.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jBRegistrarme.setForeground(new java.awt.Color(0, 153, 153));
        jBRegistrarme.setText("Registrarme");
        jBRegistrarme.setBorder(null);
        jBRegistrarme.setBorderPainted(false);
        jBRegistrarme.setContentAreaFilled(false);
        jBRegistrarme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBRegistrarmeActionPerformed(evt);
            }
        });

        jBIngresar.setBackground(new java.awt.Color(0, 153, 153));
        jBIngresar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jBIngresar.setForeground(new java.awt.Color(255, 255, 255));
        jBIngresar.setText("Ingresar");
        jBIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBIngresarActionPerformed(evt);
            }
        });

        jBCancelar.setBackground(new java.awt.Color(0, 153, 153));
        jBCancelar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jBCancelar.setForeground(new java.awt.Color(255, 255, 255));
        jBCancelar.setText("Cancelar");

        javax.swing.GroupLayout jPFormularioLayout = new javax.swing.GroupLayout(jPFormulario);
        jPFormulario.setLayout(jPFormularioLayout);
        jPFormularioLayout.setHorizontalGroup(
            jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularioLayout.createSequentialGroup()
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPFormularioLayout.createSequentialGroup()
                        .addGap(162, 162, 162)
                        .addComponent(jLTitulo))
                    .addGroup(jPFormularioLayout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPFormularioLayout.createSequentialGroup()
                                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLTexto)
                                    .addComponent(jLUsuario, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLContraseña, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(10, 10, 10)
                                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTFUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTFContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPFormularioLayout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPFormularioLayout.createSequentialGroup()
                                        .addGap(41, 41, 41)
                                        .addComponent(jBRegistrarme))
                                    .addComponent(JBOlvideContrasena))))))
                .addContainerGap(50, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPFormularioLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jBIngresar)
                .addGap(64, 64, 64)
                .addComponent(jBCancelar)
                .addGap(93, 93, 93))
        );
        jPFormularioLayout.setVerticalGroup(
            jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularioLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLTitulo)
                .addGap(83, 83, 83)
                .addComponent(jLTexto)
                .addGap(40, 40, 40)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLUsuario)
                    .addComponent(jTFUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLContraseña)
                    .addComponent(jTFContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(JBOlvideContrasena)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBRegistrarme)
                .addGap(60, 60, 60)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBIngresar)
                    .addComponent(jBCancelar))
                .addContainerGap(59, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPFondoLayout = new javax.swing.GroupLayout(jPFondo);
        jPFondo.setLayout(jPFondoLayout);
        jPFondoLayout.setHorizontalGroup(
            jPFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPFondoLayout.createSequentialGroup()
                .addGap(0, 400, Short.MAX_VALUE)
                .addComponent(jPFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPFondoLayout.setVerticalGroup(
            jPFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBRegistrarmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBRegistrarmeActionPerformed
        UsuarioRegistrarVista registrarusuario = new UsuarioRegistrarVista();
        registrarusuario.setVisible(true);
        dispose();
    }//GEN-LAST:event_jBRegistrarmeActionPerformed

    private void jBIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBIngresarActionPerformed
        InicioVista inicio = new InicioVista();
        inicio.setVisible(true);
        dispose();
    }//GEN-LAST:event_jBIngresarActionPerformed

    public static void main(String args[]) {
                try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InicioVista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(() -> {
            new LoginVista().setVisible(true);
        });
    

    }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBOlvideContrasena;
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBIngresar;
    private javax.swing.JButton jBRegistrarme;
    private javax.swing.JLabel jLContraseña;
    private javax.swing.JLabel jLTexto;
    private javax.swing.JLabel jLTitulo;
    private javax.swing.JLabel jLUsuario;
    private javax.swing.JPanel jPFondo;
    private javax.swing.JPanel jPFormulario;
    private javax.swing.JTextField jTFContrasena;
    private javax.swing.JTextField jTFUsuario;
    // End of variables declaration//GEN-END:variables
}
