
package webfly.Vsitas;
import webfly.Utileria.FondoPanel;

public class UsuarioRegistrarVista extends javax.swing.JFrame {

    public UsuarioRegistrarVista() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField7 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jPFondo = new FondoPanel("/webfly/Imagenes/FonfoLogin.jpg");
        jPFormulario = new javax.swing.JPanel();
        jLTitulo = new javax.swing.JLabel();
        jLTexto = new javax.swing.JLabel();
        jLNombre = new javax.swing.JLabel();
        jLCedula = new javax.swing.JLabel();
        jTFNombre = new javax.swing.JTextField();
        jTFCedula = new javax.swing.JTextField();
        jBGuardar = new javax.swing.JButton();
        jBCancelar = new javax.swing.JButton();
        jLContrasena = new javax.swing.JLabel();
        jTFCorreo = new javax.swing.JTextField();
        jTFContraseña = new javax.swing.JTextField();
        jLCorreo = new javax.swing.JLabel();
        jLApellido = new javax.swing.JLabel();
        jLTelefono = new javax.swing.JLabel();
        jTFTelefono = new javax.swing.JTextField();
        jTFApellido = new javax.swing.JTextField();
        jLConfirmar = new javax.swing.JLabel();
        jTFConfirmar = new javax.swing.JTextField();

        jTextField7.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel7.setText("Nombre");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel9.setText("Contraseña");

        jTextField5.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel8.setText("Cedula");

        jTextField6.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Iniciar Seción");
        setExtendedState(6);

        jPFormulario.setBackground(new java.awt.Color(255, 255, 255));
        jPFormulario.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLTitulo.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLTitulo.setForeground(new java.awt.Color(0, 153, 153));
        jLTitulo.setText("Agencia de Viajes ");

        jLTexto.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLTexto.setText("Registra tus datos ");

        jLNombre.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLNombre.setText("Nombre");

        jLCedula.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLCedula.setText("Cédula");

        jTFNombre.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jTFCedula.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jBGuardar.setBackground(new java.awt.Color(0, 153, 153));
        jBGuardar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jBGuardar.setForeground(new java.awt.Color(255, 255, 255));
        jBGuardar.setText("Guardar");
        jBGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGuardarActionPerformed(evt);
            }
        });

        jBCancelar.setBackground(new java.awt.Color(0, 153, 153));
        jBCancelar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jBCancelar.setForeground(new java.awt.Color(255, 255, 255));
        jBCancelar.setText("Cancelar");
        jBCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCancelarActionPerformed(evt);
            }
        });

        jLContrasena.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLContrasena.setText("Contraseña");

        jTFCorreo.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jTFContraseña.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jLCorreo.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLCorreo.setText("Correo");

        jLApellido.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLApellido.setText("Apellido");

        jLTelefono.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLTelefono.setText("Teléfono");

        jTFTelefono.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jTFApellido.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jLConfirmar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLConfirmar.setText("Confirmar");

        jTFConfirmar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        javax.swing.GroupLayout jPFormularioLayout = new javax.swing.GroupLayout(jPFormulario);
        jPFormulario.setLayout(jPFormularioLayout);
        jPFormularioLayout.setHorizontalGroup(
            jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularioLayout.createSequentialGroup()
                .addGap(166, 166, 166)
                .addComponent(jLTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPFormularioLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPFormularioLayout.createSequentialGroup()
                        .addGap(143, 143, 143)
                        .addComponent(jBGuardar)
                        .addGap(64, 64, 64)
                        .addComponent(jBCancelar)
                        .addGap(82, 82, 82))
                    .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPFormularioLayout.createSequentialGroup()
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLCedula)
                                .addComponent(jLNombre))
                            .addGap(41, 41, 41)
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTFNombre)
                                .addComponent(jTFCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLTelefono)
                                .addComponent(jLApellido))
                            .addGap(28, 28, 28)
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTFApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTFTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jLTexto)
                        .addGroup(jPFormularioLayout.createSequentialGroup()
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLCorreo)
                                .addComponent(jLContrasena))
                            .addGap(18, 18, 18)
                            .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPFormularioLayout.createSequentialGroup()
                                    .addComponent(jTFContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLConfirmar)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTFConfirmar))
                                .addComponent(jTFCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(21, 21, 21))
        );
        jPFormularioLayout.setVerticalGroup(
            jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormularioLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLTitulo)
                .addGap(115, 115, 115)
                .addComponent(jLTexto)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPFormularioLayout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLCedula)
                            .addComponent(jTFCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLTelefono)
                            .addComponent(jTFTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPFormularioLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTFNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLNombre)
                            .addComponent(jTFApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLApellido))))
                .addGap(27, 27, 27)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLCorreo)
                    .addComponent(jTFCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLContrasena)
                    .addComponent(jTFContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLConfirmar)
                    .addComponent(jTFConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(85, 85, 85)
                .addGroup(jPFormularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBGuardar)
                    .addComponent(jBCancelar))
                .addContainerGap(151, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPFondoLayout = new javax.swing.GroupLayout(jPFondo);
        jPFondo.setLayout(jPFondoLayout);
        jPFondoLayout.setHorizontalGroup(
            jPFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPFondoLayout.createSequentialGroup()
                .addGap(0, 341, Short.MAX_VALUE)
                .addComponent(jPFormulario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPFondoLayout.setVerticalGroup(
            jPFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFormulario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCancelarActionPerformed
        LoginVista login = new LoginVista();
        dispose();
        login.setVisible(true);
    }//GEN-LAST:event_jBCancelarActionPerformed

    private void jBGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGuardarActionPerformed
        InicioVista inicio = new InicioVista();
        dispose();
        inicio.setVisible(true);
    }//GEN-LAST:event_jBGuardarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBCancelar;
    private javax.swing.JButton jBGuardar;
    private javax.swing.JLabel jLApellido;
    private javax.swing.JLabel jLCedula;
    private javax.swing.JLabel jLConfirmar;
    private javax.swing.JLabel jLContrasena;
    private javax.swing.JLabel jLCorreo;
    private javax.swing.JLabel jLNombre;
    private javax.swing.JLabel jLTelefono;
    private javax.swing.JLabel jLTexto;
    private javax.swing.JLabel jLTitulo;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPFondo;
    private javax.swing.JPanel jPFormulario;
    private javax.swing.JTextField jTFApellido;
    private javax.swing.JTextField jTFCedula;
    private javax.swing.JTextField jTFConfirmar;
    private javax.swing.JTextField jTFContraseña;
    private javax.swing.JTextField jTFCorreo;
    private javax.swing.JTextField jTFNombre;
    private javax.swing.JTextField jTFTelefono;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    // End of variables declaration//GEN-END:variables
}
