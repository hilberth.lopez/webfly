/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.persistencia;

import java.util.List;
import webfly.BD.Usuario;

/**
 *
 * @author Luis Santiago
 */
public interface IusuarioDAO {
    public void insertar(Usuario usuario)throws Exception;
    public void modificar(Usuario usuario)throws Exception;
    public void eliminar(Usuario usuario)throws Exception;
    public Usuario consultaporcedula(Integer cedula)throws Exception;
    public List<Usuario> consultar()throws Exception;
}
