/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.persistencia;

import java.util.List;
import javax.persistence.Query;
import webfly.BD.Ciudades;

/**
 *
 * @author Luis Santiago
 */
public class ciudadDAO implements IciudadDAO {

    @Override
    public void insertar(Ciudades ciudades) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().persist(ciudades);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void modificar(Ciudades ciudades) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().merge(ciudades);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void eliminar(Ciudades ciudades) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().remove(ciudades);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Ciudades consultaporid(Integer idciudad) throws Exception {
        try {
            return EntityManagerHelper.getEntityManager().find(Ciudades.class, idciudad);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Ciudades> consultar() throws Exception {
        try {
            Query query = EntityManagerHelper.getEntityManager().createNamedQuery("Ciudades.findAll");
            return query.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }

}
