/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.persistencia;

import java.util.List;
import javax.persistence.Query;
import webfly.BD.Usuario;

/**
 *
 * @author Luis Santiago
 */
public class usuarioDAO implements IusuarioDAO {

    @Override
    public void insertar(Usuario usuario) throws Exception {
        
        try{
    EntityManagerHelper.getEntityManager().persist(usuario);
}catch(Exception e){
    throw e;
}
       
        
    }

    @Override
    public void modificar(Usuario usuario) throws Exception {
        
             try{
    EntityManagerHelper.getEntityManager().merge(usuario);
}catch(Exception e){
    throw e;
}
       
    }
    

    @Override
    public void eliminar(Usuario usuario) throws Exception {
       
                      try{
    EntityManagerHelper.getEntityManager().remove(usuario);
}catch(Exception e){
    throw e;
}
       
    }

    @Override
    public Usuario consultaporcedula(Integer cedula) throws Exception {
        
               try{
  return  EntityManagerHelper.getEntityManager().find(Usuario.class, cedula);
}catch(Exception e){
    throw e;
}
        
    }

    @Override
    public List<Usuario> consultar() throws Exception {
        
          try{
            Query query = EntityManagerHelper.getEntityManager().createNamedQuery("Usuario.findAll");
            return query.getResultList();
        }catch(Exception e){
            throw e;
        }
    }
    
}
