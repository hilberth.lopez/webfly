/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webfly.persistencia;
import java.util.List;
import webfly.BD.Ciudades;
/**
 *
 * @author Luis Santiago
 */
public interface IciudadDAO {
   
    public void insertar(Ciudades ciudades)throws Exception;
    public void modificar(Ciudades ciudades)throws Exception;
    public void eliminar(Ciudades ciudades)throws Exception;
    public Ciudades consultaporid(Integer idciudad)throws Exception;
    public List<Ciudades> consultar()throws Exception;
    
}
